import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AuthenticatedRoute from './AuthenticatedRoute.js';
import HeaderComponent from './HeaderComponent.js';
import FooterComponent from './FooterComponent.js';
import LoginComponent from './LoginComponent.js';
import ErrorComponent from './ErrorComponent';
import WelcomeComponent from './WelcomeComponent';
import ListTodosComponent from './ListTodosComponent';
import LogoutComponent from './LogoutComponent';
import TodoComponent from "./TodoComponent.js";

class TodoApp extends Component {
    render() {
        return (
            <div className="TodoApp">
                <BrowserRouter>
                    <HeaderComponent />
                    <Switch>
                        <Route exact path="/" component={LoginComponent} />
                        <Route path="/login" component={LoginComponent} />
                        <AuthenticatedRoute path="/welcome/:name" component={WelcomeComponent} />
                        <AuthenticatedRoute path="/todos/:id" component={TodoComponent} />
                        <AuthenticatedRoute path="/todos" component={ListTodosComponent} />
                        <AuthenticatedRoute path="/logout" component={LogoutComponent} />
                        <Route component={ErrorComponent} />
                    </Switch>
                    <FooterComponent />
                </BrowserRouter>
            </div>
        )
    }
}

export default TodoApp;