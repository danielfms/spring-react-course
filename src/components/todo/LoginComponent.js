import React, {Component} from 'react';
import AuthenticationService from './AuthenticationService';

class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            showSuccessMessage: false,
            hasLoginFailed: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);
    }

    handleChange(e) {
        console.log(e.target.name);
        this.setState({ [e.target.name]: e.target.value });
    }

    loginClicked() {
        AuthenticationService.executeJwtAthenticationService(this.state.username, this.state.password)
        .then((response)=>{
            AuthenticationService.registerSuccessfullLoginForJwt(this.state.username, response.data.token);
            this.props.history.push(`/welcome/${this.state.username}`);
        }).catch((e)=>{
            this.setState({showSuccessMessage:false});
            this.setState({hasLoginFailed:true});
        })
    }

    render() {
        return (
            <div>
                <h1>Login</h1>
                <div className="container">
                    <ShowInvalidCredential hasLoginFailed={this.state.hasLoginFailed} />
                    <ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage} />
                Username: <input type="text" name="username" value={this.state.username} onChange={this.handleChange} />
                Password: <input type="password" name="password" value={this.state.password} onChange={this.handleChange} />
                    <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
                </div>

            </div>
        )
    }
}

function ShowInvalidCredential(props) {
    if (props.hasLoginFailed) {
        return <div className="alert alert-warning">Invalid Credentials</div>
    }
    return null;
}

function ShowLoginSuccessMessage(props) {
    if (props.showSuccessMessage) {
        return <div>Login Successful</div>
    }
    return null;
}

export default LoginComponent;