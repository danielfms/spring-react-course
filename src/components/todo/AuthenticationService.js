import axios from "axios";
import {API_URL} from "./../../Constants";

export const USER_NAME_SESSION_ATTRIBUTE_NAME = "authenticatedUser";

class AuthenticationService {

    executeJwtAthenticationService(username, password){
        return axios.post(`${API_URL}/authenticate`,{username,password});
    }

    registerSuccessfullLogin(username){
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
    }

    registerSuccessfullLoginForJwt(username,token){
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        this.setupAxiosInterceptors(this.createJwtToken(token));
    }


    logout(){
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    }

    isUserLoggedIn(){
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        if(user === null) return false;
        return true;
    }

    getLoggedInUsername(){
        if(this.isUserLoggedIn()){
            return sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        }
        return '';
    }

    createJwtToken(token){
        return 'Bearer ' + token;
    }

    setupAxiosInterceptors(token){
        axios.interceptors.request.use(
            (config) => {
                if(this.isUserLoggedIn()){
                    config.headers.authorization = token
                }
                return config
            }
        )
    }
}

export default new AuthenticationService();