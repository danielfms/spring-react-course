import React, {Component} from 'react';
import AuthenticationSerivice from './AuthenticationService';
import {Route, Redirect} from 'react-router-dom';

class AuthenticatedRoute extends Component{
    render(){
        if(AuthenticationSerivice.isUserLoggedIn()){
            return <Route {...this.props} />
        }else{
            return <Redirect to="/login" />
        }
    }
}


export default AuthenticatedRoute;