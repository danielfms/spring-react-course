import React, { Component } from "react";
import moment from "moment";
import { Formik, Field, Form, ErrorMessage } from "formik";
import TodoDataSercice from "./../../api/todo/TodoDataService.js";
import AuthenticationService from "./AuthenticationService.js";

class TodoComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            description: 'Learn Forms 2',
            targetDate: moment(new Date()).format("YYYY-MM-DD")
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.validate = this.validate.bind(this);
    }

    componentDidMount() {
        let username = AuthenticationService.getLoggedInUsername();
        if (this.state.id === -1) {
            return;
        }
        TodoDataSercice.retrieveTodo(username, this.state.id)
            .then(response => {
                console.log(response)
                this.setState({
                    description: response.data.description,
                    targetDate: moment(response.data.targetDate).format("YYYY-MM-DD")
                })
            })
    }

    validate(values) {
        let errors = {};
        if (!values.description) {
            errors.description = 'Enter a description';
        } else if (values.description.length < 5) {
            errors.description = 'Enter atleast 5 Characters in Description'
        }

        if (!moment(values.targetDate).isValid()) {
            errors.targetDate = 'Enter a valid Target Date'

        }
        return errors;
    }

    onSubmit(values) {
        let username = AuthenticationService.getLoggedInUsername();
        let todo = {
            id: this.state.id,
            description: values.description,
            targetDate: values.targetDate
        };
        if (this.state.id === -1) {
            TodoDataSercice.createTodo(username, todo).then(() => this.props.history.push('/todos'))
        } else {
            TodoDataSercice.updateTodo(username, this.state.id, todo).then(() => this.props.history.push('/todos'))
        }
    }

    render() {
        let { description, targetDate } = this.state;
        return (
            <div>
                <h1>Todo</h1>
                <div className="container">
                    <Formik
                        initialValues={{ description, targetDate }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="description" component="div" className="alert alert-warning" />
                                    <ErrorMessage name="targetDate" component="div" className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>description</label>
                                        <Field className="form-control" type="text" name="description" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Target Date</label>
                                        <Field className="form-control" type="date" name="targetDate" />
                                    </fieldset>
                                    <button className="btn btn-success" type="submit">Submit</button>
                                </Form>
                            )

                        }
                    </Formik>
                </div>
            </div>
        )
    }
}

export default TodoComponent;